package com.devcamp.relationship.model;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class CProduct {
	public CProduct() {
		super();
	}

	public CProduct(String name, long price) {
		super();
		this.name = name;
		this.price = price;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name="price")
	private long price;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {
			CascadeType.PERSIST,
			CascadeType.MERGE
	}, mappedBy = "products")
	private Set<COrder> orders;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public Set<COrder> getOrders() {
		return orders;
	}

	public void setOrders(Set<COrder> orders) {
		this.orders = orders;
	}

}
